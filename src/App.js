import React,{Component} from "react";
import './App.css';
import Number from "./components/number/number";

class App extends Component {
    state = {
        Numbers: [
            {number: 1},
            {number: 2},
            {number: 3},
            {number: 4},
            {number: 5},
        ]
    };

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    arrayGenerate = () => {
        let arr = [];
        let num = 0;
        do {
            num = this.getRandomInt(5, 36)
            if (!arr.includes(num)) {
                arr.push(num);
            }
        } while (arr.length < 5);
        arr.sort(function(a, b){return a - b});
        return arr
    }

    changeNumbers = () => {
        const numbers = [...this.state.Numbers];
        const arr = this.arrayGenerate()
        for (let i = 0; i < 5; i++) {
            numbers[i].number = arr[i];
        }
        this.setState({numbers});
    };

    render() {
        return (
            <div className="App">
                <div>
                    <button onClick={this.changeNumbers}>New numbers</button>
                </div>
                <Number number={this.state.Numbers[0].number}/>
                <Number number={this.state.Numbers[1].number}/>
                <Number number={this.state.Numbers[2].number}/>
                <Number number={this.state.Numbers[3].number}/>
                <Number number={this.state.Numbers[4].number}/>
            </div>
        );
    }
}

export default App;
