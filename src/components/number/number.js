import React from 'react';
import './number.css';

const Number = props => {
    return(
        <div className="number">
            <div>{props.number}</div>
        </div>
    );
}

export default Number;